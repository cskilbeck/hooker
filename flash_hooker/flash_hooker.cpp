//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "resource.h"
#include "licenses.h"
#include "..\flash_hooker_dll\minhook\include\MinHook.h"
#include "..\flash_hooker_dll\kiero.h"
#include "..\flash_hooker_dll\flash_hooker_dll.h"

//////////////////////////////////////////////////////////////////////

#if defined(_WIN64)
char const *d3d9_text = "D3D9 - 64 bit";
char const *d3d11_text = "D3D11 - 64 bit";
#else
char const *d3d9_text = "D3D9 - 32 bit";
char const *d3d11_text = "D3D11 - 32 bit";
#endif

//////////////////////////////////////////////////////////////////////

void set_d3d(HWND dlg, UINT id_on, UINT id_off, char const *window_txt, kiero::RenderType new_render_type)
{
    HMENU menu = GetMenu(dlg);
    set_render_type(new_render_type);
    CheckMenuItem(menu, id_off, MF_UNCHECKED);
    CheckMenuItem(menu, id_on, MF_CHECKED);
    SetWindowText(dlg, window_txt);
}

void set_d3d9_mode(HWND hDlg)
{
    log("Setting D3D9 mode");
    set_d3d(hDlg, ID_D3D_9, ID_D3D_11, d3d9_text, kiero::RenderType::D3D9);
}

void set_d3d11_mode(HWND hDlg)
{
    log("Setting D3D11 mode");
    set_d3d(hDlg, ID_D3D_11, ID_D3D_9, d3d11_text, kiero::RenderType::D3D11);
}

//////////////////////////////////////////////////////////////////////

INT_PTR CALLBACK dlg_proc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg) {

    case WM_INITDIALOG:
        install_kbd_hook(hDlg);
        set_d3d11_mode(hDlg);
        SetTimer(hDlg, 1, 10, null);
        break;

    case WM_TIMER:
        KillTimer(hDlg, 1);
        SendMessage(GetDlgItem(hDlg, IDC_EDIT1), EM_SETSEL, -1, -1);    // ffs remove selection from edit control
        break;

    case WM_SIZE:
    case WM_SIZING: {
        RECT r;
        HWND edit_ctrl = GetDlgItem(hDlg, IDC_EDIT1);
        GetClientRect(hDlg, &r);
        MoveWindow(edit_ctrl, 0, 0, r.right, r.bottom, TRUE);
    } break;

    case WM_COMMAND:
        switch(LOWORD(wParam)) {
        case ID_D3D_9:
            set_d3d9_mode(hDlg);
            break;
        case ID_D3D_11:
            set_d3d11_mode(hDlg);
            break;
        case ID_LICENSE_KIERO:
            log_raw(licenses::text());
            break;
        }
        break;

    case WM_CLOSE:
        uninstall_kbd_hook();
        EndDialog(hDlg, 0);
        break;
    }

    return false;
}

//////////////////////////////////////////////////////////////////////

int APIENTRY WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
    DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_WININFO), NULL, dlg_proc, (LPARAM)NULL);
    return 0;
}
