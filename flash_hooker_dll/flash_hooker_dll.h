#pragma once

#if defined(UNICODE)
#error Unicode not supported
#endif

#ifdef DLL_EXPORTS
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif

constexpr nullptr_t null = nullptr;

extern DLL_API bool is_hooked;

extern DLL_API void set_render_type(kiero::RenderType render_type);
extern DLL_API bool install_kbd_hook(HWND dlg);
extern DLL_API void uninstall_kbd_hook();
extern DLL_API void log(char const *text, ...);
extern DLL_API void va_log(char const *text, va_list v);
extern DLL_API void log_raw(char const *text);
