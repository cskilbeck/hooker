//////////////////////////////////////////////////////////////////////

#include <Windows.h>
#include <d3d9.h>
#include <dxgi.h>
#include <d3d10_1.h>
#include <d3d10.h>
#include <d3d11.h>

#include "minhook/include/MinHook.h"
#include "kiero.h"
#include "flash_hooker_dll.h"

//////////////////////////////////////////////////////////////////////

typedef LPDIRECT3D9(__stdcall *Direct3DCreate9_FN)(uint32_t);

typedef long(__stdcall *D3D11CreateDeviceAndSwapChain_FN)(IDXGIAdapter *, D3D_DRIVER_TYPE, HMODULE, UINT, const D3D_FEATURE_LEVEL *, UINT, UINT, const DXGI_SWAP_CHAIN_DESC *,
                                                          IDXGISwapChain **, ID3D11Device **, D3D_FEATURE_LEVEL *, ID3D11DeviceContext **);

//////////////////////////////////////////////////////////////////////

static kiero::RenderType g_renderType = kiero::RenderType::D3D11;

// See METHODSTABLE.txt for more information
static uint_ptr *g_methodsTable = null;

//////////////////////////////////////////////////////////////////////

kiero::Status kiero::init(RenderType _renderType)
{
    WNDCLASSEX windowClass;
    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = DefWindowProc;
    windowClass.cbClsExtra = 0;
    windowClass.cbWndExtra = 0;
    windowClass.hInstance = GetModuleHandle(NULL);
    windowClass.hIcon = NULL;
    windowClass.hCursor = NULL;
    windowClass.hbrBackground = NULL;
    windowClass.lpszMenuName = NULL;
    windowClass.lpszClassName = "Kiero";
    windowClass.hIconSm = NULL;

    RegisterClassEx(&windowClass);

    HWND window = CreateWindow(windowClass.lpszClassName, "Kiero DirectX Window", WS_OVERLAPPEDWINDOW, 0, 0, 100, 100, NULL, NULL, windowClass.hInstance, NULL);

    switch(_renderType) {

        //////////////////////////////////////////////////////////////////////

    case RenderType::D3D9: {

        auto return_status = Status::UnknownError;

        LPDIRECT3D9 direct3D9 = null;
        LPDIRECT3DDEVICE9 device = null;

        HMODULE libD3D9 = GetModuleHandle("d3d9.dll");
        if(libD3D9 == null) {
            return_status = Status::ModuleNotFoundError;
            goto cleanup_d3d9;
        }

        Direct3DCreate9_FN Direct3DCreate9 = reinterpret_cast<Direct3DCreate9_FN>(GetProcAddress(libD3D9, "Direct3DCreate9"));
        if(Direct3DCreate9 == null) {
            return_status = Status::FunctionNotFound;
            goto cleanup_d3d9;
        }

        direct3D9 = Direct3DCreate9(D3D_SDK_VERSION);
        if(direct3D9 == NULL) {
            return_status = Status::CreateD3DFailed;
            goto cleanup_d3d9;
        }

        D3DDISPLAYMODE displayMode;
        if(direct3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode) < 0) {
            return_status = Status::GetDisplayModeFailed;
            goto cleanup_d3d9;
        }

        D3DPRESENT_PARAMETERS params;
        params.BackBufferWidth = 0;
        params.BackBufferHeight = 0;
        params.BackBufferFormat = displayMode.Format;
        params.BackBufferCount = 0;
        params.MultiSampleType = D3DMULTISAMPLE_NONE;
        params.MultiSampleQuality = NULL;
        params.SwapEffect = D3DSWAPEFFECT_DISCARD;
        params.hDeviceWindow = window;
        params.Windowed = 1;
        params.EnableAutoDepthStencil = 0;
        params.AutoDepthStencilFormat = D3DFMT_UNKNOWN;
        params.Flags = NULL;
        params.FullScreen_RefreshRateInHz = 0;
        params.PresentationInterval = 0;

        if(direct3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, window, D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_DISABLE_DRIVER_MANAGEMENT, &params, &device) < 0) {
            return_status = Status::CreateDeviceFailed;
            goto cleanup_d3d9;
        }

        g_methodsTable = (uint_ptr *)calloc(119, sizeof(uint_ptr));

        // this check is here to satisfy the linter, in practice it's not necessary and if the alloc fails we are basically fucked anyway
        if(g_methodsTable == null) {
            return_status = Status::AllocFailed;
            goto cleanup_d3d9;
        }

        memcpy(g_methodsTable, *(uint_ptr **)device, 119 * sizeof(uint_ptr));
        MH_Initialize();
        g_renderType = RenderType::D3D9;
        return_status = Status::Success;

    cleanup_d3d9:

        if(device != null) {
            device->Release();
            device = NULL;
        }

        if(direct3D9 != null) {
            direct3D9->Release();
            direct3D9 = NULL;
        }

        g_renderType = RenderType::D3D9;

        DestroyWindow(window);
        UnregisterClass(windowClass.lpszClassName, windowClass.hInstance);

        return return_status;
    }

        //////////////////////////////////////////////////////////////////////

    case RenderType::D3D11: {

        HMODULE libD3D11 = null;

        D3D11CreateDeviceAndSwapChain_FN D3D11CreateDeviceAndSwapChain_PTR = null;
        IDXGISwapChain *swapChain = null;
        ID3D11Device *device = null;
        ID3D11DeviceContext *context = null;

        auto return_status = Status::UnknownError;

        if((libD3D11 = GetModuleHandle("d3d11.dll")) == null) {
            return_status = Status::ModuleNotFoundError;
            goto cleanup_d3d11;
        }

        if((D3D11CreateDeviceAndSwapChain_PTR = reinterpret_cast<D3D11CreateDeviceAndSwapChain_FN>(GetProcAddress(libD3D11, "D3D11CreateDeviceAndSwapChain"))) == null) {
            return_status = Status::FunctionNotFound;
            goto cleanup_d3d11;
        }

        D3D_FEATURE_LEVEL featureLevel;
        const D3D_FEATURE_LEVEL featureLevels[] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_11_1 };

        DXGI_RATIONAL refreshRate;
        refreshRate.Numerator = 60;
        refreshRate.Denominator = 1;

        DXGI_MODE_DESC bufferDesc;
        bufferDesc.Width = 100;
        bufferDesc.Height = 100;
        bufferDesc.RefreshRate = refreshRate;
        bufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        bufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
        bufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

        DXGI_SAMPLE_DESC sampleDesc;
        sampleDesc.Count = 1;
        sampleDesc.Quality = 0;

        DXGI_SWAP_CHAIN_DESC swapChainDesc;
        swapChainDesc.BufferDesc = bufferDesc;
        swapChainDesc.SampleDesc = sampleDesc;
        swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        swapChainDesc.BufferCount = 1;
        swapChainDesc.OutputWindow = window;
        swapChainDesc.Windowed = 1;
        swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
        swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

        if(D3D11CreateDeviceAndSwapChain_PTR(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevels, _countof(featureLevels), D3D11_SDK_VERSION, &swapChainDesc, &swapChain,
                                             &device, &featureLevel, &context) < 0) {
            return_status = Status::CreateDeviceFailed;
            goto cleanup_d3d11;
        }

        g_methodsTable = (uint_ptr *)calloc(205, sizeof(uint_ptr));
        if(g_methodsTable == null) {
            return_status = Status::AllocFailed;
            goto cleanup_d3d11;
        }
        memcpy(g_methodsTable, *(uint_ptr **)swapChain, 18 * sizeof(uint_ptr));
        memcpy(g_methodsTable + 18, *(uint_ptr **)device, 43 * sizeof(uint_ptr));
        memcpy(g_methodsTable + 18 + 43, *(uint_ptr **)context, 144 * sizeof(uint_ptr));
        MH_Initialize();
        g_renderType = RenderType::D3D11;
        return_status = Status::Success;

    cleanup_d3d11:

        if(swapChain != null) {
            swapChain->Release();
            swapChain = null;
        }

        if(device != null) {
            device->Release();
            device = null;
        }

        if(context != null) {
            context->Release();
            context = null;
        }

        DestroyWindow(window);
        UnregisterClass(windowClass.lpszClassName, windowClass.hInstance);

        return return_status;
    }
    }

    return Status::NotSupportedError;
}

//////////////////////////////////////////////////////////////////////

void kiero::shutdown()
{
    if(g_renderType > 0) {
        MH_Uninitialize();
        free(g_methodsTable);
        g_methodsTable = NULL;
        g_renderType = RenderType::None;
    }
}

//////////////////////////////////////////////////////////////////////

kiero::RenderType kiero::getRenderType()
{
    return g_renderType;
}

//////////////////////////////////////////////////////////////////////

uint_ptr *kiero::getMethodsTable()
{
    return g_methodsTable;
}

//////////////////////////////////////////////////////////////////////

MH_STATUS kiero::bind(uint16_t _index, void **_original, void *_function)
{
    if(g_renderType > 0) {
        MH_STATUS c = MH_CreateHook((void *)g_methodsTable[_index], _function, _original);
        if(c != MH_OK) {
            return c;
        }
        return MH_EnableHook((void *)g_methodsTable[_index]);
    }
    return MH_ERROR_NOT_INITIALIZED;
}
